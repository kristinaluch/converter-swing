package main.java.com.swingconverter.service;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ConverterGUIService extends JFrame {
    private static final String [] timeList = {"Min", "Hour", "Day", "Week", "Month", "Astronomical Year"};
    private static final String [] weightList = {"g", "carat", "eng pound", "pound", "stone", "rus pound"};
    private static final String [] volumeList = {"m^3", "gallon", "pint", "quart", "barrel", "cubic foot", "cubic inch"};
    private static final String [] lengthList = {"km", "mile", "nautical mile", "cable", "league", "foot", "yard"};
    private static final String [] CList = {"K (Kelvin scale)", "F (Fahrenheit scale)", "Re (Reaumur scale)",
            "Ro (Romer scale)", "Ra (Rankine scale)", "N (Newtonian scale)", "D (Delisle scale)"};


    public JLabel timeTitle = new JLabel("Time");
    public JLabel secLabel = new JLabel("sec ");
    public JTextField secText = new JTextField("", 10);
    public JComboBox timeBox = new JComboBox(timeList);
    public JButton convertTimeButton = new JButton("Convert");
    public JLabel resTimeLabel = new JLabel("result ");
    public JTextField timeText = new JTextField("", 10);
    public JButton backTimeButton = new JButton("Back");

    public JLabel weightTitle = new JLabel("Weight");
    public JLabel kgLabel = new JLabel("kg ");
    public JTextField kgText = new JTextField("", 10);
    public JComboBox weightBox = new JComboBox(weightList);
    public JButton convertWeightButton = new JButton("Convert");
    public JLabel resWeightLabel = new JLabel("result ");
    public JTextField weightText = new JTextField("", 10);
    public JButton backWeightButton = new JButton("Back");

    public JLabel volumeTitle = new JLabel("Volume");
    public JLabel lLabel = new JLabel("l ");
    public JTextField lText = new JTextField("", 10);
    public JComboBox volumeBox = new JComboBox(volumeList);
    public JButton convertVolumeButton = new JButton("Convert");
    public JLabel resVolumeLabel = new JLabel("result");
    public JTextField volumeText = new JTextField("", 10);
    public JButton backVolumeButton = new JButton("Back");

    public JLabel lengthTitle = new JLabel("Length");
    public JLabel mLabel = new JLabel("m ");
    public JTextField mText = new JTextField("", 10);
    public JComboBox lengthBox = new JComboBox(lengthList);
    public JButton convertLengthButton = new JButton("Convert");
    public JLabel resLengthLabel = new JLabel("result");
    public JTextField lengthText = new JTextField("", 10);
    public JButton backLengthButton = new JButton("Back");

    public JLabel temperatureTitle = new JLabel("Temperature");
    public JLabel CLabel = new JLabel("C ");
    public JTextField CText = new JTextField("", 10);
    public JComboBox temperatureBox = new JComboBox(CList);
    public JButton convertTemperatureButton = new JButton("Convert");
    public JLabel resTemperatureLabel = new JLabel("result");
    public JTextField temperatureText = new JTextField("", 10);
    public JButton backTemperatureButton = new JButton("Back");

    public ConverterGUIService(){
        setTitle("Converter");
        Font font = new Font("SansSerif", Font.BOLD, 18),
        fontTitle = new Font("SansSerif", Font.BOLD, 23);
        setSize(700, 590);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = getContentPane();
        container.setLayout(null);
        container.setBackground(new Color(159, 149, 196));

        timeTitle.setBounds(330, 15, 55, 30);
        timeTitle.setFont(fontTitle);
        container.add(timeTitle);
        secLabel.setBounds(25, 55, 40, 30);
        secLabel.setFont(font);
        container.add(secLabel);
        secText.setBounds(70, 55, 100, 30);
        container.add(secText);
        timeBox.setBounds(175, 55, 130, 30);
        container.add(timeBox);
        convertTimeButton.setBounds(310, 55, 95, 30);
        convertTimeButton.addActionListener(new Converter());
        container.add(convertTimeButton);
        resTimeLabel.setBounds(415, 55, 55, 30);
        resTimeLabel.setFont(font);
        container.add(resTimeLabel);
        timeText.setBounds(480, 55, 110, 30);
        container.add(timeText);
        backTimeButton.setBounds(595, 55, 65, 30);
        backTimeButton.addActionListener(new Converter());
        container.add(backTimeButton);

        weightTitle.setBounds(320, 115, 80, 30);
        weightTitle.setFont(fontTitle);
        container.add(weightTitle);
        kgLabel.setBounds(25, 155, 40, 30);
        kgLabel.setFont(font);
        container.add(kgLabel);
        kgText.setBounds(70, 155, 100, 30);
        container.add(kgText);
        weightBox.setBounds(175, 155, 130, 30);
        container.add(weightBox);
        convertWeightButton.setBounds(310, 155, 95, 30);
        convertWeightButton.addActionListener(new Converter());
        container.add(convertWeightButton);
        resWeightLabel.setBounds(415, 155, 55, 30);
        resWeightLabel.setFont(font);
        container.add(resWeightLabel);
        weightText.setBounds(480, 155, 110, 30);
        container.add(weightText);
        backWeightButton.setBounds(595, 155, 65, 30);
        backWeightButton.addActionListener(new Converter());
        container.add(backWeightButton);

        volumeTitle.setBounds(320, 215, 85, 30);
        volumeTitle.setFont(fontTitle);
        container.add(volumeTitle);
        lLabel.setBounds(25, 255, 40, 30);
        lLabel.setFont(font);
        container.add(lLabel);
        lText.setBounds(70, 255, 100, 30);
        container.add(lText);
        volumeBox.setBounds(175, 255, 130, 30);
        container.add(volumeBox);
        convertVolumeButton.setBounds(310, 255, 95, 30);
        convertVolumeButton.addActionListener(new Converter());
        container.add(convertVolumeButton);
        resVolumeLabel.setBounds(415, 255, 55, 30);
        resVolumeLabel.setFont(font);
        container.add(resVolumeLabel);
        volumeText.setBounds(480, 255, 110, 30);
        container.add(volumeText);
        backVolumeButton.setBounds(595, 255, 65, 30);
        backVolumeButton.addActionListener(new Converter());
        container.add(backVolumeButton);

        lengthTitle.setBounds(320, 315, 85, 30);
        lengthTitle.setFont(fontTitle);
        container.add(lengthTitle);
        mLabel.setBounds(25, 355, 40, 30);
        mLabel.setFont(font);
        container.add(mLabel);
        mText.setBounds(70, 355, 100, 30);
        container.add(mText);
        lengthBox.setBounds(175, 355, 130, 30);
        container.add(lengthBox);
        convertLengthButton.setBounds(310, 355, 95, 30);
        convertLengthButton.addActionListener(new Converter());
        container.add(convertLengthButton);
        resLengthLabel.setBounds(415, 355, 55, 30);
        resLengthLabel.setFont(font);
        container.add(resLengthLabel);
        lengthText.setBounds(480, 355, 110, 30);
        container.add(lengthText);
        backLengthButton.setBounds(595, 355, 65, 30);
        backLengthButton.addActionListener(new Converter());
        container.add(backLengthButton);

        temperatureTitle.setBounds(290, 415, 145, 30);
        temperatureTitle.setFont(fontTitle);
        container.add(temperatureTitle);
        CLabel.setBounds(25, 455, 40, 30);
        CLabel.setFont(font);
        container.add(CLabel);
        CText.setBounds(70, 455, 100, 30);
        container.add(CText);
        temperatureBox.setBounds(175, 455, 130, 30);
        container.add(temperatureBox);
        convertTemperatureButton.setBounds(310, 455, 95, 30);
        convertTemperatureButton.addActionListener(new Converter());
        container.add(convertTemperatureButton);
        resTemperatureLabel.setBounds(415, 455, 55, 30);
        resTemperatureLabel.setFont(font);
        container.add(resTemperatureLabel);
        temperatureText.setBounds(480, 455, 110, 30);
        container.add(temperatureText);
        backTemperatureButton.setBounds(595, 455, 65, 30);
        backTemperatureButton.addActionListener(new Converter());
        container.add(backTemperatureButton);

        setContentPane(container);
    }

    class Converter implements ActionListener {
        public void actionPerformed(ActionEvent event) {

            final JButton source = (JButton)event.getSource();

            if(source.equals(convertTimeButton)){
                convertTime();
            } else
                if(source.equals(backTimeButton)){
                    backTime();
                } else
                    if(source.equals(convertWeightButton)){
                        convertWeight();
                    } else
                        if(source.equals(backWeightButton)){
                            backWeight();
                        } else
                        if(source.equals(convertVolumeButton)){
                            convertVolume();
                        } else
                        if(source.equals(backVolumeButton)){
                            backVolume();
                        } else
                        if(source.equals(convertLengthButton)){
                            convertLength();
                        } else
                        if(source.equals(backLengthButton)){
                            backLength();
                        } else
                        if(source.equals(convertTemperatureButton)){
                            convertTemperature();
                        } else
                        if(source.equals(backTemperatureButton)){
                            backTemperature();
                        }
        }

        public void convertTime(){
            double second = Double.parseDouble(secText.getText());
            String selectTime = String.valueOf(timeBox.getSelectedItem());

            if(selectTime.equals("Min")){
                timeText.setText(String.valueOf(second / 60));
            } else if(selectTime.equals("Hour")){
                timeText.setText(String.valueOf(second / 3600));
            } else if(selectTime.equals("Day")){
                timeText.setText(String.valueOf(second / 86400));
            } else if(selectTime.equals("Week")){
                timeText.setText(String.valueOf(second / 604800));
            } else if(selectTime.equals("Month")){
                timeText.setText(String.valueOf(second / 2678400));
            } else if(selectTime.equals("Astronomical Year")){
                timeText.setText(String.valueOf(second / 32140800));
            }
        }

        public void backTime(){
            double time = Double.parseDouble(timeText.getText());
            String selectTime = String.valueOf(timeBox.getSelectedItem());

            if(selectTime.equals("Min")){
                secText.setText(String.valueOf(time * 60));
            } else if(selectTime.equals("Hour")){
                secText.setText(String.valueOf(time * 3600));
            } else if(selectTime.equals("Day")){
                secText.setText(String.valueOf(time * 86400));
            } else if(selectTime.equals("Week")){
                secText.setText(String.valueOf(time * 604800));
            } else if(selectTime.equals("Month")){
                secText.setText(String.valueOf(time * 2678400));
            } else if(selectTime.equals("Astronomical Year")){
                secText.setText(String.valueOf(time * 32140800));
            }
        }

        public void convertWeight(){
            double kg = Double.parseDouble(kgText.getText());
            String selectWeight = String.valueOf(weightBox.getSelectedItem());

            if(selectWeight.equals("g")){
                weightText.setText(String.valueOf(kg  * 1000));
            } else if(selectWeight.equals("carat")){
                weightText.setText(String.valueOf(kg * 5000));
            } else if(selectWeight.equals("eng pound")){
                weightText.setText(String.valueOf(kg * 0.45359237));
            } else if(selectWeight.equals("pound")){
                weightText.setText(String.valueOf(kg * 2.20462262185));
            } else if(selectWeight.equals("stone")){
                weightText.setText(String.valueOf(kg * 0.157473));
            } else if(selectWeight.equals("rus pound")){
                weightText.setText(String.valueOf(kg * 2.441899995502));
            }
        }

        public void backWeight(){
            double weight = Double.parseDouble(weightText.getText());
            String selectWeight = String.valueOf(weightBox.getSelectedItem());

            if(selectWeight.equals("g")){
                kgText.setText(String.valueOf(weight / 1000));
            } else if(selectWeight.equals("carat")){
                kgText.setText(String.valueOf(weight / 5000));
            } else if(selectWeight.equals("eng pound")){
                kgText.setText(String.valueOf(weight / 0.45359237));
            } else if(selectWeight.equals("pound")){
                kgText.setText(String.valueOf(weight / 2.20462262185));
            } else if(selectWeight.equals("stone")){
                kgText.setText(String.valueOf(weight / 0.157473));
            } else if(selectWeight.equals("rus pound")){
                kgText.setText(String.valueOf(weight / 2.441899995502));
            }
        }

        public void convertVolume(){
            double l = Double.parseDouble(lText.getText());
            String selectVolume = String.valueOf(volumeBox.getSelectedItem());

            if(selectVolume.equals("m^3")){
                volumeText.setText(String.valueOf(l  * 0.001));
            } else if(selectVolume.equals("gallon")){
                volumeText.setText(String.valueOf(l * 0.26417205));
            } else if(selectVolume.equals("pint")){
                volumeText.setText(String.valueOf(l * 1.7597504));
            } else if(selectVolume.equals("quart")){
                volumeText.setText(String.valueOf(l * 1.05669));
            } else if(selectVolume.equals("barrel")){
                volumeText.setText(String.valueOf(l * 0.00628981));
            } else if(selectVolume.equals("cubic foot")){
                volumeText.setText(String.valueOf(l * 0.03531467));
            } else if(selectVolume.equals("cubic inch")){
                volumeText.setText(String.valueOf(l * 61.023744));
            }
        }

        public void backVolume(){
            double volume = Double.parseDouble(volumeText.getText());
            String selectVolume = String.valueOf(volumeBox.getSelectedItem());

            if(selectVolume.equals("m^3")){
                lText.setText(String.valueOf(volume / 0.001));
            } else if(selectVolume.equals("gallon")){
                lText.setText(String.valueOf(volume / 0.26417205));
            } else if(selectVolume.equals("pint")){
                lText.setText(String.valueOf(volume / 1.7597504));
            } else if(selectVolume.equals("quart")){
                lText.setText(String.valueOf(volume / 1.05669));
            } else if(selectVolume.equals("barrel")){
                lText.setText(String.valueOf(volume / 0.00628981));
            } else if(selectVolume.equals("cubic foot")){
                lText.setText(String.valueOf(volume / 0.03531467));
            } else if(selectVolume.equals("cubic inch")){
                lText.setText(String.valueOf(volume / 61.023744));
            }
        }

        public void convertLength(){
            double m = Double.parseDouble(mText.getText());
            String selectLength = String.valueOf(lengthBox.getSelectedItem());

            if(selectLength.equals("km")){
                lengthText.setText(String.valueOf(m  / 1000));
            } else if(selectLength.equals("mile")){
                lengthText.setText(String.valueOf(m * 0.000621371));
            } else if(selectLength.equals("nautical mile")){
                lengthText.setText(String.valueOf(m * 0.0005399568));
            } else if(selectLength.equals("cable")){
                lengthText.setText(String.valueOf(m * 0.0053961182483768));
            } else if(selectLength.equals("league")){
                lengthText.setText(String.valueOf(m * 0.0002071237));
            } else if(selectLength.equals("foot")){
                lengthText.setText(String.valueOf(m * 3.280839895));
            } else if(selectLength.equals("yard")){
                lengthText.setText(String.valueOf(m * 1.0936132983));
            }
        }

        public void backLength(){
            double length = Double.parseDouble(lengthText.getText());
            String selectLength = String.valueOf(lengthBox.getSelectedItem());

            if(selectLength.equals("km")){
                mText.setText(String.valueOf(length  * 1000));
            } else if(selectLength.equals("mile")){
                mText.setText(String.valueOf(length / 0.000621371));
            } else if(selectLength.equals("nautical mile")){
                mText.setText(String.valueOf(length / 0.0005399568));
            } else if(selectLength.equals("cable")){
                mText.setText(String.valueOf(length / 0.0053961182483768));
            } else if(selectLength.equals("league")){
                mText.setText(String.valueOf(length / 0.0002071237));
            } else if(selectLength.equals("foot")){
                mText.setText(String.valueOf(length / 3.280839895));
            } else if(selectLength.equals("yard")){
                mText.setText(String.valueOf(length / 1.0936132983));
            }
        }

        public void convertTemperature(){
            double C = Double.parseDouble(CText.getText());
            String selectTemperature = String.valueOf(temperatureBox.getSelectedItem());

            if(selectTemperature.equals("K (Kelvin scale)")){
                temperatureText.setText(String.valueOf(C + 273.15));
            } else if(selectTemperature.equals("F (Fahrenheit scale)")){
                temperatureText.setText(String.valueOf(C * 9 / 5 + 32));
            } else if(selectTemperature.equals("Re (Reaumur scale)")){
                temperatureText.setText(String.valueOf(C * 4 / 5));
            } else if(selectTemperature.equals("Ro (Romer scale)")){
                temperatureText.setText(String.valueOf(C * 0.525 + 7.5));
            } else if(selectTemperature.equals("Ra (Rankine scale)")){
                temperatureText.setText(String.valueOf((C * 273.15) * 9 / 5));
            } else if(selectTemperature.equals("N (Newtonian scale)")){
                temperatureText.setText(String.valueOf(C * 0.33));
            } else if(selectTemperature.equals("D (Delisle scale)")){
                temperatureText.setText(String.valueOf(C * 1.5 - 100));
            }
        }

        public void backTemperature(){
            double temperature = Double.parseDouble(temperatureText.getText());
            String selectTemperature = String.valueOf(temperatureBox.getSelectedItem());

            if(selectTemperature.equals("K (Kelvin scale)")){
                CText.setText(String.valueOf(temperature - 273.5));
            } else if(selectTemperature.equals("F (Fahrenheit scale)")){
                CText.setText(String.valueOf((temperature - 32) * 5 / 9));
            } else if(selectTemperature.equals("Re (Reaumur scale)")){
                CText.setText(String.valueOf(temperature * 5 / 4));
            } else if(selectTemperature.equals("Ro (Romer scale)")){
                CText.setText(String.valueOf((temperature - 7.5) / 0.525));
            } else if(selectTemperature.equals("Ra (Rankine scale)")){
                CText.setText(String.valueOf((temperature - 491.67) * 5 / 9));
            } else if(selectTemperature.equals("N (Newtonian scale)")){
                CText.setText(String.valueOf(temperature / 0.33));
            } else if(selectTemperature.equals("D (Delisle scale)")){
                CText.setText(String.valueOf((temperature + 100) / 1.5));
            }
        }
    }
}
